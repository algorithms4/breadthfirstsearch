﻿using System;
using System.Collections.Generic;

namespace BreadthFirstSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Initializing the tree.
            
            /*
            * Let's create such a tree.
            *                        1
            *             2                    3
            *    4                5    6                7
            */
            
            var tree =  new Node
            {
                Value = 1,
                Childs = new List<Node>
                {
                    new Node
                    {
                        Value = 2,
                        Childs = new List<Node>
                        {
                            new Node
                            {
                                Value = 4,
                            },
                            new Node
                            {
                                Value = 5,
                            
                            }
                        }
                    },
                    new Node
                    {
                        Value = 3,
                        Childs = new List<Node>
                        {
                            new Node
                            {
                                Value = 6,
                            },
                            new Node
                            {
                                Value = 7,
                            
                            }
                        }
                    }
                }
            };
            #endregion

            tree.GetNodes().ForEach(Console.WriteLine);
        }
    }
}
