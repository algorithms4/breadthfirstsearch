﻿using System.Collections.Generic;
using System.Linq;

namespace BreadthFirstSearch
{
    public static class NodeExtensions
    {
        public static List<Node> GetNodes(this Node root)
        {
            var result = new List<Node>();
            var queue = new Queue<Node>();
            queue.Enqueue(root);
            while (queue.Count != 0)
            {
                var node = queue.Dequeue();                
                node.Childs?.ForEach(x => queue.Enqueue(x));
                result.Add(node);
            }
            return result;
        }
    }
}