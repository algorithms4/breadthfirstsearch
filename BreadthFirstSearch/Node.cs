﻿using System.Collections.Generic;
using System.Linq;

namespace BreadthFirstSearch
{
    public class Node
    {
        public List<Node> Childs { get; set; }

        public int Value { get; set; }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}